import csv
import pymorphy2
import re

morph = pymorphy2.MorphAnalyzer()


def fraction(arr: list) -> list:
    len_arr = len(arr)
    last_precent = 0
    for i in range(len_arr):
        arr[0] = re.sub(r'[^\w\s]', '', arr[0])
        for j in arr[0].split():
            arr.append(j)
        arr.remove(arr[0])
        if last_precent != i * 100 // len_arr:
            last_precent = i * 100 // len_arr
            print('готово', last_precent, '%')
    return arr


def lemmatize(words: list) -> list:
    words = fraction(words)
    res = []

    for word in words:
        p = morph.parse(word)[0]
        res.append(p.normal_form)

    return res


def del_stopwords(lemmatize_text: list, stopwords: list) -> list:
    clear_lemmatize = []
    for i in lemmatize_text:
        if not(i in stopwords):
            clear_lemmatize.append(i)
    return clear_lemmatize


def count_words(lemmatize_text: list) -> dict:
    dict_count_words = {}

    for i in lemmatize_text:
        if i in dict_count_words:
            dict_count_words[i] += 1
        else:
            dict_count_words[i] = 1

    return dict_count_words


def del_words_come_across_less_five(lemmatize_text: list, dict_count_words: dict) -> list:
    clear_lemmatize = []
    for i in lemmatize_text:
        if dict_count_words[i] >= 5:
            clear_lemmatize.append(i)
        else:
            del dict_count_words[i]
    return clear_lemmatize


def recording_frequencies_in_csv_file(lemmatize_text: list, dict_count_words: dict):
    with open(f'word_frequency.csv', 'w', encoding='utf-8') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(['word', 'frequency'])
        for i in lemmatize_text:
            writer.writerow([i, dict_count_words[i]])
