import pandas as pd
from funcs import *


def main():
    df = pd.read_csv('for_covid_tweets/covid_tweets.csv.gz', compression='gzip')
    print('файл прочитан')

    print("Идет процесс обработки файла")
    lemmatize_text = lemmatize(df['text'].tolist())

    with open("for_covid_tweets/stopwords-filter-ru.txt", 'r', encoding='utf-8') as file:
        stopwords = file.read().split('\n')

    print("Начинается процесс удаления стоп слов")
    lemmatize_text = del_stopwords(lemmatize_text, stopwords)
    print('Стоп слова удалены')

    print("Начинается процесс подсчета слов")
    dict_count_words = count_words(lemmatize_text)

    lemmatize_text = set(lemmatize_text)
    lemmatize_text = list(lemmatize_text)

    print("Начинается процесс удаления слов встречающихся меньше 5 раз")
    lemmatize_text = del_words_come_across_less_five(lemmatize_text, dict_count_words)
    print('Слова встречающиеся меньше 5 раз удаленны')

    lemmatize_text.sort(key=lambda x: -dict_count_words[x])

    print("Начинается процесс создания файла с таблицой частот")
    recording_frequencies_in_csv_file(lemmatize_text, dict_count_words)
    print('файл с таблицой частот был создан')


if __name__ == '__main__':
    main()
