import gensim
import pandas as pd
from my_dictionary import MyDictionary


def generate_model(common_corpus: list, common_dictionary, num_topics: int):
    lda = gensim.models.ldamodel.LdaModel(corpus=common_corpus, id2word=common_dictionary, num_topics=num_topics)

    with open(f"model/{num_topics}_topics.txt", "w", encoding='utf-8') as file:
        for i in lda.show_topics(num_topics, 20, formatted=False):
            for j in i:
                if type(j) == int:
                    file.write(str(j) + "\t")
                else:
                    for q in j:
                        file.write(str(q[0]) + " ")
            file.write("\n")


def main():
    df = pd.read_csv('word_frequency.csv')

    dict_f = {}
    for i, j in zip(df['word'].tolist(), df['frequency'].tolist()):
        dict_f[i] = j

    common_dictionary = MyDictionary([df["word"].tolist()])

    common_corpus = [common_dictionary.doc2bow(document=text, numbers=dict_f) for text in [df["word"].tolist()]]

    generate_model(common_corpus, common_dictionary, 20)
    generate_model(common_corpus, common_dictionary, 30)
    generate_model(common_corpus, common_dictionary, 50)


if __name__ == '__main__':
    main()
